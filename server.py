import os
import logging
from flask import Flask
from flask import request
from flask import render_template
from flask import url_for
from fgraph import draw_graph
from fgraph import FGraph
from logging.handlers import RotatingFileHandler

DIR = os.path.dirname(__file__)
DUMPS_DIR = os.path.join(DIR, 'dumps')
IMAGES_DIR = os.path.join(DIR, 'static', 'img')
app = Flask(__name__)


@app.route('/')
def start_page():
    return render_template('index.html')


@app.route('/egonet')
def egonet_handler():

    user_id = request.args.get('user_id')
    app.logger.info(request)
    app.logger.info(request.remote_addr)

    if not user_id:
        return start_page()

    fgraph = FGraph(int(user_id), DUMPS_DIR)
    image_path = url_for('static', filename=('img/' + user_id + '.png'))
    draw_graph(fgraph, os.path.join(IMAGES_DIR, user_id + '.png'))

    return render_template('egonet.html',
                           user=user_id,
                           name=fgraph.user_name(),
                           year=fgraph.predict_year(),
                           city=fgraph.predict_city(),
                           students=fgraph.students_names(),
                           classmates=fgraph.classmates_names(),
                           image=image_path,
                           )

if __name__ == "__main__":

    handler = RotatingFileHandler(os.path.join(DIR, 'server.log'),
                                  maxBytes=10000, backupCount=1)
    handler.setLevel(logging.INFO)
    app.logger.addHandler(handler)

    app.run(debug=True)
    #app.run(host='0.0.0.0', port=80)
