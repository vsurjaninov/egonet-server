import os
import pickle

from collections import defaultdict
from collections import Counter

import numpy as np
import pandas as pd
import networkx as nx
import community


from vkapi import VkAPI
api = VkAPI()


def _load_egonet(user_id, dumps_dir):
    try:
        return pickle.load(open(os.path.join(dumps_dir, str(user_id) + '.pickle'), 'r'))
    except IOError:
        egonet = api.get_user_network(user_id, 2)
        pickle.dump(egonet, open(os.path.join(dumps_dir, str(user_id) + '.pickle'), 'w'))
        return egonet


def _load_profiles(F, user_id, dumps_dir):
    try:
        return pickle.load(open(os.path.join(dumps_dir, str(user_id) + '_profiles.pickle'), 'r'))
    except IOError:
        profiles = api.get_user_profiles(F.nodes())
        pickle.dump(profiles, open(os.path.join(dumps_dir, str(user_id) + '_profiles.pickle'), 'w'))
        return profiles


def draw_graph(fgraph, path):

    import matplotlib
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt

    plt.figure(figsize=(10, 10))
    values = [fgraph.part.get(node) for node in fgraph.F.nodes()]
    nx.draw_spring(fgraph.F, cmap=plt.get_cmap('jet'), node_color=values, node_size=200, with_labels=False)
    plt.savefig(path)


class FGraph(object):
    def __init__(self, user_id, dumps_dir):
        self.id = user_id
        self.egonet = _load_egonet(user_id, dumps_dir)
        self.F = nx.Graph()

        first_level = set(self.egonet[user_id]['friends'])
        for friend_id in first_level:
            next_friends = set(self.egonet[friend_id]['friends'])    
            commons = next_friends & first_level
            if len(commons) > 0:
                self.F.add_node(friend_id)
                for com_pr_id in commons:
                    self.F.add_edge(friend_id, com_pr_id)

        nx.transitivity(self.F)

        self.part = community.best_partition(self.F)
        self.communities = defaultdict(list)
        for person_id, com_id in self.part.iteritems():
            self.communities[com_id].append(person_id)

        self.profiles = _load_profiles(self.F, user_id, dumps_dir)
        self.prdf = pd.DataFrame(self.profiles, columns=["id", "first_name", "last_name",
                                                         "bdate", "sex", "city", "universities",
                                                         "graduation", "schools"])
        self.prdf['community'] = self.prdf["id"].map(self.part)
        self.prdf.index = self.prdf["id"]
        self.prdf = self.prdf.drop("id", axis=1)
        self.prdf.sort_index(inplace=True)

    def user_name(self):
        return self.egonet[self.id]['first_name'] + ' ' + self.egonet[self.id]['last_name']

    def _max_count_com(self, df, field):
        com_vals = defaultdict(int)
        for com_id in self.communities:
            most_common = Counter(df.loc[df['community'] == com_id, field]).most_common()
            if most_common:
                val, count = most_common[0]
                com_vals[com_id] = count
        return max(com_vals, key=com_vals.get)

    def university_com(self):
        match_university = lambda unvs: unvs[0]['name'] if unvs else np.nan
        universities = self.prdf[self.prdf['universities'].notnull()][['universities', 'community']]
        universities['university'] = universities['universities'].map(match_university)
        universities = universities.drop('universities', axis=1)
        universities = universities[universities['university'].notnull()]

        return self._max_count_com(universities, 'university')

    def school_com(self):
        match_school = lambda sch: sch[-1]['name'] if sch else np.nan
        schools = self.prdf[self.prdf['schools'].notnull()][['schools', 'community']]
        schools['school'] = schools['schools'].map(match_school)
        schools = schools.drop('schools', axis=1)
        schools = schools[schools['school'].notnull()]

        return self._max_count_com(schools, 'school')

    def _members_names(self, com_id):
        profiles = map(lambda x: (x['id'], list(x.iteritems())), self.profiles)
        profiles = dict(profiles)
        names = list()
        for pr_id in self.communities[com_id]:
            try:
                profile = dict(profiles[pr_id])
                names.append(profile['first_name'] + ' ' + profile['last_name'])
            except KeyError:
                pass
        return names

    def students_names(self):
        return self._members_names(self.university_com())

    def classmates_names(self):
        return self._members_names(self.school_com())

    def predict_year(self):

        def match_year(date):
            date = str(date).split('.')
            if len(date) == 3:
                return int(date[2])
            else:
                return np.nan

        years = self.prdf[self.prdf['bdate'].notnull()][['bdate', 'community']]
        years['year'] = self.prdf['bdate'].map(match_year)
        years = years.drop('bdate', axis=1)
        years = years[years['year'].notnull()]

        com_id = self._max_count_com(years, 'year')

        return int(Counter(years.loc[years['community'] == com_id, 'year']).most_common(1)[0][0])

    def predict_city(self):
        cities = self.prdf[self.prdf['city'].notnull()][['city', 'community']]
        cities['city'] = cities['city'].map(lambda c: c['title'])
        com_id = self._max_count_com(cities, 'city')

        return Counter(cities.loc[cities['community'] == com_id, 'city']).most_common(1)[0][0]
